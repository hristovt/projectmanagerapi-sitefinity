﻿using SitefinityWebApp.CustomResources;
using SitefinityWebApp.Mvc.Models;
using SitefinityWebApp.Mvc.Models.Fields;
using SitefinityWebApp.SfOverrides.PropertyDescriptors;
using System;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Frontend;
using Telerik.Sitefinity.Frontend.DynamicContent.Mvc.Models;
using Telerik.Sitefinity.Frontend.Forms.Mvc.Models.Fields.CheckboxesField;
using Telerik.Sitefinity.Frontend.Forms.Mvc.Models.Fields.DropdownListField;
using Telerik.Sitefinity.Frontend.Mvc.Infrastructure.Controllers;
using Telerik.Sitefinity.Localization;
using Telerik.Sitefinity.Mvc.Proxy;
using Telerik.Sitefinity.Frontend.Mvc.Models;
using Telerik.Sitefinity.Services;
using System.Web.UI;
using SitefinityWebApp.Core.DynamicSubmitForm.ProjectSubmit;
using Telerik.Sitefinity.Data.Utilities.Exporters;
using Telerik.Microsoft.Practices.Unity;
using SitefinityWebApp.SfOverrides.Exporters;
using Telerik.Sitefinity.Configuration;

namespace SitefinityWebApp
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            Bootstrapper.Bootstrapped += this.Bootstrapper_Bootstrapped;
        }

        private void Bootstrapper_Bootstrapped(object sender, EventArgs e)
        {          
            Project.RegisterEvents();

            #region Custom resources
            Res.RegisterResource<ProjectManagerResources>();
            Res.RegisterResource<ProjectResources>();
            #endregion

            #region Register Configs
            Config.RegisterSection<Configuration.VSRPEmailNotificationsConfig>();
            #endregion

            SitefinityWebApp.Core.ProjectManagerAPI.Tasks.ProjectManagerApiNotifier.Schedule();
        }

        
    }
}
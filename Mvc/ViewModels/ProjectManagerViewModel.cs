﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Mvc.ViewModels
{
    public class ProjectManagerViewModel
    {
        public string DisplayName { get; set; }
        public string JobTitle { get; set; }
        public string ImageUrl { get; set; }
        public string Introduction { get; set; }
    }
}
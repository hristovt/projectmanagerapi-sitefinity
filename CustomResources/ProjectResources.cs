﻿using Telerik.Sitefinity.Localization;

namespace SitefinityWebApp.CustomResources
{    
    [ObjectInfo("ProjectResources", ResourceClassId = "ProjectResources", Title = "ProjectResourcesTitle", TitlePlural = "ProjectResourcesTitlePlural", Description = "ProjectResourcesDescription")]
    public class ProjectResources : Resource
    {
        [ResourceEntry(nameof(FullTypeName), Value = "Telerik.Sitefinity.DynamicTypes.Model.LatestNews.Latestnews", Description = "label: Telerik.Sitefinity.DynamicTypes.Model.LatestNews.Latestnews", LastModified = "2020/01/30")]
        public string FullTypeName
        {
            get { return this[nameof(this.FullTypeName)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesTitle), Value = "Project Resources", Description = "label: Project Resources", LastModified = "2020/02/25")]
        public string ProjectResourcesTitle
        {
            get { return this[nameof(this.ProjectResourcesTitle)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesTitlePlural), Value = "Project Resources", Description = "label: Project Resources", LastModified = "2020/02/25")]
        public string ProjectResourcesTitlePlural
        {
            get { return this[nameof(this.ProjectResourcesTitlePlural)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesDescription), Value = "Latest News Resources", Description = "label: Latest News Resources", LastModified = "2020/02/25")]
        public string ProjectResourcesDescription
        {
            get { return this[nameof(this.ProjectResourcesDescription)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesAcademicProgram), Value = "Academic Program:", Description = "label: Academic Program:", LastModified = "2020/02/25")]
        public string ProjectResourcesAcademicProgram
        {
            get { return this[nameof(this.ProjectResourcesAcademicProgram)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesTotalResult), Value = "Total Result(s) Found:", Description = "label: Total Result(s) Found:", LastModified = "2020/02/25")]
        public string ProjectResourcesTotalResult
        {
            get { return this[nameof(this.ProjectResourcesTotalResult)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesApply), Value = "Apply", Description = "label: Apply", LastModified = "2020/03/09")]
        public string ProjectResourcesApply
        {
            get { return this[nameof(this.ProjectResourcesApply)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesProjectDescription), Value = "Project Description", Description = "label: Project Description", LastModified = "2020/03/09")]
        public string ProjectResourcesProjectDescription
        {
            get { return this[nameof(this.ProjectResourcesProjectDescription)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesProgram), Value = "Program -", Description = "label: Program -", LastModified = "2020/03/09")]
        public string ProjectResourcesProgram
        {
            get { return this[nameof(this.ProjectResourcesProgram)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesDivision), Value = "Division -", Description = "label: Division -", LastModified = "2020/03/09")]
        public string ProjectResourcesDivision
        {
            get { return this[nameof(this.ProjectResourcesDivision)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesAboutThe), Value = "About the", Description = "label: About the", LastModified = "2020/03/09")]
        public string ProjectResourcesAboutThe
        {
            get { return this[nameof(this.ProjectResourcesAboutThe)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesProjectManager), Value = "ProjectManager", Description = "label: ProjectManager", LastModified = "2020/03/09")]
        public string ProjectResourcesProjectManager
        {
            get { return this[nameof(this.ProjectResourcesProjectManager)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesDesiredProjectDeliverables), Value = "Desired Project Deliverables", Description = "label: Desired Project Deliverables", LastModified = "2020/03/09")]
        public string ProjectResourcesDesiredProjectDeliverables
        {
            get { return this[nameof(this.ProjectResourcesDesiredProjectDeliverables)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesFacultyLabLink), Value = "Faculty Lab Link -", Description = "label: Faculty Lab Link ", LastModified = "2020/05/11")]
        public string ProjectResourcesFacultyLabLink
        {
            get { return this[nameof(this.ProjectResourcesFacultyLabLink)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesFieldOfStudy), Value = "Field of Study -", Description = "label: Field of Study ", LastModified = "2020/05/11")]
        public string ProjectResourcesFieldOfStudy
        {
            get { return this[nameof(this.ProjectResourcesFieldOfStudy)]; }
        }
        [ResourceEntry(nameof(ProjectResourcesCenterAffiliation), Value = "Center Affiliation -", Description = "label: Center Affiliation ", LastModified = "2020/05/11")]
        public string ProjectResourcesCenterAffiliation
        {
            get { return this[nameof(this.ProjectResourcesCenterAffiliation)]; }
        }
    }
}
﻿using Telerik.Sitefinity.Localization;

namespace SitefinityWebApp.CustomResources
{
    [ObjectInfo("ProjectManagerResources", ResourceClassId = "ProjectManagerResources", Title = "ProjectManagerResourcesTitle", TitlePlural = "ProjectManagerResourcesTitlePlural", Description = "ProjectManagerResourcesDescription")]
    public class ProjectManagerResources : Resource
    {
        [ResourceEntry(nameof(ProjectManagerResourcesTitle), Value = "ProjectManager Resources", Description = "label: ProjectManager Resources", LastModified = "2020/01/30")]
        public string ProjectManagerResourcesTitle
        {
            get { return this[nameof(this.ProjectManagerResourcesTitle)]; }
        }
        [ResourceEntry(nameof(ProjectManager), Value = "ProjectManager", Description = "label: ProjectManager", LastModified = "2020/01/30")]
        public string ProjectManager
        {
            get { return this[nameof(this.ProjectManager)]; }
        }
        [ResourceEntry(nameof(AboutThe), Value = "About the", Description = "label: About the", LastModified = "2020/01/30")]
        public string AboutThe
        {
            get { return this[nameof(this.AboutThe)]; }
        }
    }
}
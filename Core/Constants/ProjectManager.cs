﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Utilities.TypeConverters;

namespace SitefinityWebApp.Core.Constants
{
    public class ProjectManager
    {
        /// <summary>
        /// Fully qualified type name for the ProjectManager type.
        /// </summary>
        public const string FullTypeName = "Telerik.Sitefinity.DynamicTypes.Model.ProjectManagers.ProjectManager";

        /// <summary>
        /// Resolved System.Type for the ProjectManager type.
        /// </summary>
        public static readonly Type ResolvedType = TypeResolutionService.ResolveType(FullTypeName);

        public const string ProjectManagerAPIUrl = "https://www.myapiurl.com";

        public const string ProjectManagerApiCacheKey = "ProjectManagerApiData";

        public class ProfileFields
        {
            public const string ReadFromVSRP = "ReadFromVSRP";
        }
    }
}
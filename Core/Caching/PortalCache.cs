﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Microsoft.Practices.EnterpriseLibrary.Caching;
using Telerik.Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using Telerik.Sitefinity.Services;

namespace SitefinityWebApp.Core.Caching
{
    public class PortalCache
    {
        private static ICacheManager _cacheManager;

        private static ICacheManager CacheManager
        {
            get
            {
                if (_cacheManager == null)
                {
                    _cacheManager = SystemManager.GetCacheManager(CacheManagerInstance.Global);
                }
                return _cacheManager;
            }
        }

        public static void SaveToCache(string cacheKey, object savedItem)
        {
            CacheManager.Add(cacheKey, savedItem);
        }

        public static void SaveToCache(string cacheKey, object savedItem, ICacheItemExpiration itemExpiration)
        {
            CacheManager.Add(cacheKey, savedItem, CacheItemPriority.Normal, null, itemExpiration);
        }

        public static T GetFromCache<T>(string cacheKey) where T : class
        {
            return CacheManager.GetData(cacheKey) as T;
        }

        public static void RemoveFromCache(string cacheKey)
        {
            CacheManager.Remove(cacheKey);
        }

        public static bool IsIncache(string cacheKey)
        {
            return CacheManager.GetData(cacheKey) != null;
        }        
    }
}
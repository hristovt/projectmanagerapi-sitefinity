﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using SitefinityWebApp.Core.Caching;
using SitefinityWebApp.Core.Constants;
using SitefinityWebApp.Core.ProjectManagerAPI.Model;
using Telerik.Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using Telerik.Sitefinity.Configuration;

namespace SitefinityWebApp.Core.ProjectManagerAPI
{
    public class ProjectManagerAPIManager
    {
        public static ProjectManagerAPIModel GetProjectManagerAPIContent(string url, string email)
        { 
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentNullException("url");
            }
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentNullException("email");
            }

            var data = GetData(url);

            return data.Where(r => r.Email.ToLower() == email).FirstOrDefault();
        }

        public static IEnumerable<ProjectManagerAPIModel> GetData(string url)
        {
            var configInterval = Config.Get<Configuration.VSRPEmailNotificationsConfig>().ApiCallIntervalInMinutes;
            var data = PortalCache.GetFromCache<IEnumerable<ProjectManagerAPIModel>>(ProjectManager.ProjectManagerApiCacheKey);
            if (data == null)
            {
                lock (objectLock)
                {
                    data = PortalCache.GetFromCache<IEnumerable<ProjectManagerAPIModel>>(ProjectManager.ProjectManagerApiCacheKey);
                    if (data == null)
                    {
                        HttpClient httpClient = new HttpClient();
                        var jsonString = httpClient.GetStringAsync(url).Result;

                        if (!string.IsNullOrWhiteSpace(jsonString))
                        {
                            data = Newtonsoft.Json.JsonConvert.DeserializeObject<IEnumerable<ProjectManagerAPIModel>>(jsonString);
                            PortalCache.SaveToCache(ProjectManager.ProjectManagerApiCacheKey, data, new AbsoluteTime(TimeSpan.FromHours(configInterval)));
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }

            return data;
        }

        private static object objectLock = new object();
    }
}
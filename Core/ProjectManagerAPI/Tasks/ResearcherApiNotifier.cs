﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Scheduling.Model;
using Telerik.Sitefinity.Scheduling;
using Telerik.Sitefinity.Abstractions;
using SitefinityWebApp.Core.Helpers;
using SitefinityWebApp.Core.Caching;
using SitefinityWebApp.Core.ProjectManagerAPI.Model;
using SitefinityWebApp.Core.Constants;
using Telerik.Sitefinity.Configuration;
using System.Diagnostics;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Model;

namespace SitefinityWebApp.Core.ProjectManagerAPI.Tasks
{
    public class ProjectManagerApiNotifier : ScheduledTask
    {
        public const string TaskKey = "91BDFC81-FD7F-4647-98FC-91A33E1FCEC1";
        public const string CronScheduleSpecValue = "*/5 * * * *";
        private const string EmailNotificationsKey = "ProjectMembersEmailNotifications";
        private string CustomData = string.Empty;

        /// <summary>
        /// Returns the configured interval in minutes in which an email should be sent.
        /// </summary>
        public static int FetchIntervalInMinutes
        {
            get
            {
                return Config.Get<Configuration.VSRPEmailNotificationsConfig>().IntevalInMinutes;
            }
        }

        public ProjectManagerApiNotifier()
        {
            this.Key = TaskKey;
            this.ExecuteTime = DateTime.UtcNow;
            this.ScheduleSpec = CronScheduleSpecValue;
            this.ScheduleSpecType = "crontab";
        }

        public override void ExecuteTask()
        {
            Log.Write("Task: " + this.TaskName + " ran at " + DateTime.UtcNow, ConfigurationPolicy.Trace);
            var lockObj = new object();
            lock (lockObj)
            {
                var items = GetItemsWithoutEmail(ProjectManagerAPIManager.GetData(ProjectManager.ProjectManagerAPIUrl));
                Log.Write($"Task: {this.TaskName} getting items without email. Got {items.Count()} items." + DateTime.UtcNow, ConfigurationPolicy.Trace);
                if (items.Count() > 0)
                {
                    Log.Write($"Task: {this.TaskName} checking if notifications should be sent." + DateTime.UtcNow, ConfigurationPolicy.Trace);
                    if (this.ShouldNotify())
                    {
                        lock (lockObj)
                        {
                            try
                            {
                                Log.Write($"Task: {this.TaskName} sending emails." + DateTime.UtcNow, ConfigurationPolicy.Trace);
                                string emailNotification = string.Format("The following Project members do not have emails: <br/><br/> {0}",
                                    string.Join("<br/>", items.Select(i => $"{i.Title}")));
                                EmailHelper.SendEmailNotifications(emailNotification);
                            }
                            catch (Exception ex)
                            {
                                Log.Write($"Task: {this.TaskName} ERROR sending emails." + DateTime.UtcNow, ConfigurationPolicy.Trace);
                                throw;
                            }
                            finally
                            {
                                this.UpdateNotification(EmailNotificationsKey, DateTime.Now.ToUniversalTime());
                            }
                        }
                    }
                }
            }
        }

        private bool ShouldNotify()
        {
            if (!string.IsNullOrWhiteSpace(this.CustomData))
            {
                BaseScheduledTaskData data = BaseScheduledTaskData.FromJson(this.CustomData);
                if (data.LastTimeExecutions.ContainsKey(EmailNotificationsKey))
                {
                    var lastExecuted = data.LastTimeExecutions[EmailNotificationsKey];
                    if ((DateTime.Now.ToUniversalTime() - lastExecuted).Minutes <= FetchIntervalInMinutes)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
        
        private void UpdateNotification(string name, DateTime dateTimeToSet)
        {
            var taskData = BaseScheduledTaskData.FromJson(this.GetCustomData());
            if (taskData == null)
                taskData = new BaseScheduledTaskData();
            taskData.UpdateNotification(name, dateTimeToSet.ToUniversalTime());
            this.SetCustomData(BaseScheduledTaskData.ToJson(taskData));
        }

        private static IEnumerable<Model.ProjectManagerAPIModel> GetItemsWithoutEmail(IEnumerable<Model.ProjectManagerAPIModel> data)
        {
            List<Model.ProjectManagerAPIModel> result = new List<ProjectManagerAPIModel>();
            var allProfiles = UserProfileManager.GetManager().GetUserProfiles<SitefinityProfile>()
                .Where(p => !p.GetValue<bool>(Constants.ProjectManager.ProfileFields.ReadFromVSRP));
            foreach(var ProjectManager in allProfiles)
            {
                var roles = RoleManager.GetManager()
                    .GetRolesForUser(ProjectManager.User.Id);
                if (roles
                    .Any(r => r.Name == "Faculty Members"))
                {
                    if (!data.Any(apiInfo => (bool)apiInfo.Email?.Trim().Equals(ProjectManager.User.Email, StringComparison.InvariantCultureIgnoreCase)))

                    result.Add(new ProjectManagerAPIModel()
                    {
                        Email = ProjectManager.User.Email,
                        Title = $"{ProjectManager.FirstName} {ProjectManager.LastName}",
                        LastName = ProjectManager.LastName,
                        ID = ProjectManager.User.Id.ToString()
                    });
                }
            }
            return result;
        }

        public static void Schedule()
        {
            SchedulingManager schedulingManager = SchedulingManager.GetManager();

            var existingTasks = schedulingManager.GetTaskData().Where(x => x.Key == TaskKey);
            if (existingTasks.Count() > 1)
            {
                existingTasks.Skip(1).ToList()
                    .ForEach(t =>
                    {
                        schedulingManager.DeleteTaskData(t);
                    });
                schedulingManager.SaveChanges();
            }
            var existingTask = existingTasks.FirstOrDefault();

            if (existingTask == null)
            {
                ProjectManagerApiNotifier newTask = new ProjectManagerApiNotifier();
                schedulingManager.AddTask(newTask);
                SchedulingManager.RescheduleNextRun();
                schedulingManager.SaveChanges();
            }
            else if (existingTask.Status == TaskStatus.Failed ||
                    existingTask.Status == TaskStatus.Stopped ||
                    (existingTask.IsRunning &&
                    existingTask.LastModified.AddMinutes(FetchIntervalInMinutes) < DateTime.UtcNow))
            {
                var customData = existingTask.TaskData;
                schedulingManager.DeleteTaskData(existingTask);
                schedulingManager.SaveChanges();

                ProjectManagerApiNotifier newTask = new ProjectManagerApiNotifier();
                newTask.SetCustomData(customData);
                schedulingManager.AddTask(newTask);

                SchedulingManager.RescheduleNextRun();
                schedulingManager.SaveChanges();
            }
        }

        public override string TaskName
        {
            get
            {
                return this.GetType().FullName;
            }
        }

        public override void SetCustomData(string customData)
        {
            this.CustomData = customData;
        }

        public override string GetCustomData()
        {
            return this.CustomData;
        }
    }
}
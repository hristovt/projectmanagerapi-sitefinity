﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace SitefinityWebApp.Core.ProjectManagerAPI.Model
{
    public class ProjectManagerAPIModel
    {
        [JsonProperty("ID")]
        public string ID { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("LastName")]
        public string LastName { get; set; }

        [JsonProperty("JobTitleField")]
        public string JobTitle { get; set; }

        [JsonProperty("Email")]
        public string Email { get; set; }

        [JsonProperty("ImageUrl")]
        public string AvatarImageUrl { get; set; }

        [JsonProperty("ResearchInterests")]
        public string Introduction { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SitefinityWebApp.Core.ProjectManagerAPI.Model
{
    /// <summary>
    /// Represents a scheduled custom data class where you can track multiple items executions.
    /// </summary>
    public class BaseScheduledTaskData
    {
        public Dictionary<string, DateTime> LastTimeExecutions = new Dictionary<string, DateTime>();

        public BaseScheduledTaskData() { }

        public static BaseScheduledTaskData FromJson(string jsonRepresentation)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<BaseScheduledTaskData>(jsonRepresentation);
        }

        public static string ToJson(BaseScheduledTaskData data)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(data);
        }

        public virtual void UpdateNotification(string name, DateTime dateTimeToSet)
        {
            if (!this.LastTimeExecutions.ContainsKey(name))
            {
                this.LastTimeExecutions.Add(name, dateTimeToSet);
            }
            else
            {
                this.LastTimeExecutions[name] = dateTimeToSet;
            }
        }
    }
}
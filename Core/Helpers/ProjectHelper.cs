﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Frontend.Mvc.Models;
using SitefinityWebApp.Core.Constants;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Data.Linq.Dynamic;
using Telerik.Sitefinity.GenericContent.Model;
using Telerik.OpenAccess;
using Telerik.Sitefinity.Security.Model;
using Telerik.Sitefinity.Frontend.Identity.Mvc.Models.Profile;
using SitefinityWebApp.Mvc.ViewModels;
using SitefinityWebApp.Core.ProjectManagerAPI;
using SitefinityWebApp.Core.ProjectManagerAPI.Model;

namespace SitefinityWebApp.Core.Helpers
{
    public class ProjectHelper
    {
        public static ItemViewModel GetProjectProjectManagerDynamicContent(ContentDetailsViewModel model)
        {
            DynamicModuleManager mngr = DynamicModuleManager.GetManager();

            var projectOwner = ((DynamicContent)model.Item.DataItem).Owner;
            var user = UserManager.GetManager().GetUsers().FirstOrDefault(u => u.Id == projectOwner);
            if (user != null)
            {
                var ProjectManagerFound = mngr.GetDataItems(ProjectManager.ResolvedType)
                                          .FirstOrDefault(item => item.Status == ContentLifecycleStatus.Live && item.Visible && item.GetValue<string>("Username").ToLower() == user.UserName.ToLower());
                return ProjectManagerFound == null ? null : new ItemViewModel(ProjectManagerFound);
            }

            return null;
        }

        public static ProfilePreviewViewModel GetProjectProjectManager(ContentDetailsViewModel model)
        {
            DynamicModuleManager mngr = DynamicModuleManager.GetManager();

            var projectOwner = ((DynamicContent)model.Item.DataItem).Owner;
            var user = UserManager.GetManager().GetUsers().FirstOrDefault(u => u.Id == projectOwner);
            if (user != null)
            {
                var profile = UserProfileManager.GetManager().GetUserProfile<SitefinityProfile>(user);

                return profile == null ? null : new ProfilePreviewViewModel(new List<UserProfile>() { profile });
            }

            return null;
        }

        public static ProjectManagerViewModel GetProjectManagerContent(ContentDetailsViewModel model)
        {
            var ProjectManagerDynamicContent = ProjectHelper.GetProjectProjectManager(model);
            if (ProjectManagerDynamicContent != null)
            {
                var ProjectManagerAPIContent = ProjectManagerAPIManager.GetProjectManagerAPIContent(ProjectManager.ProjectManagerAPIUrl, ProjectManagerDynamicContent.Email.ToLower());
                bool readFromVSRP = ProjectManagerDynamicContent.SelectedUserProfiles.FirstOrDefault().Fields.ReadFromVSRP;
                if (ProjectManagerDynamicContent != null)
                {
                    ProjectManagerViewModel ProjectManager = new ProjectManagerViewModel()
                    {
                        DisplayName = readFromVSRP != true && ProjectManagerAPIContent != null ? ProjectManagerAPIContent.Title : ProjectManagerDynamicContent.DisplayName,
                        JobTitle = readFromVSRP != true && ProjectManagerAPIContent != null ? ProjectManagerAPIContent.JobTitle : ProjectManagerDynamicContent.SelectedUserProfiles.FirstOrDefault().Fields.JobTitle,
                        ImageUrl = readFromVSRP != true && ProjectManagerAPIContent != null ? ProjectManagerAPIContent.AvatarImageUrl : ProjectManagerDynamicContent.AvatarImageUrl,
                        Introduction = readFromVSRP != true && ProjectManagerAPIContent != null ? ProjectManagerAPIContent.Introduction : ProjectManagerDynamicContent.SelectedUserProfiles.FirstOrDefault().Fields.Introduction
                    };
                    return ProjectManager;
                };
            }
            return null;
        }      
    }
}